<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use RealRashid\SweetAlert\Facades\Alert;
use DB;
use App\Kelas;
use DataTables;

class KelasController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if ($request->ajax()) {
            $data = Kelas::get();
            return Datatables::of($data)
                    ->addIndexColumn()
                    ->addColumn('action', function($data){
     
                           $btn = '<a href="kelas/'.$data->id.'/edit" class="edit btn btn-success btn-sm">Edit</a> <a href="kelas/destroy/'.$data->id.'"  class="delete btn btn-danger btn-sm">Delete</a>';
                            return $btn;
                    })
                    ->rawColumns(['action'])
                    ->make(true);
        }
        
        return view('kelas.index');
    }


   

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('kelas.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([ 
            'name' => 'required',
        ]);

        $post = new Kelas;
        $post->nama = $request["name"];
        $post->save();
        
        Alert::success('Success!', 'Kelas Berhasil Di tambahkan');

        return redirect(route('kelas.index'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $kelas = Kelas::find($id);
        return view('kelas.edit', ['kelas' => $kelas]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'nama' => 'required'
        ]);

        $kelas = Kelas::find($id);
        $kelas->nama = $request->nama;
        $kelas->update();
        Alert::success('Berhasil', 'Data Kelas Berhasil Di Perbaharui');

        return redirect(route('kelas.index'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $kelas =  Kelas::findOrFail($id);
        $kelas->delete();

        Alert::warning('Berhasil!', 'Data Berhasil Di Hapus');

        return redirect(route('kelas.index'));
    }
}
