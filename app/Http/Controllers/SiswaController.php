<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use RealRashid\SweetAlert\Facades\Alert;
use App\siswa;
use App\Kelas;
use DataTables;
use DB;

class SiswaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $model = siswa::with('kelas');
        if ($request->ajax()) {
            $data = siswa::get();
            return Datatables::of($data)
                    ->addIndexColumn()
                    ->addColumn('Kelas',function (siswa $siswa) {
                        return $siswa->kelas->nama;})
                    ->addColumn('action', function($data){
                           $btn = '<a href="siswa/'.$data->id.'/edit" class="edit btn btn-success btn-sm">Edit</a> <a href="siswa/destroy/'.$data->id.'"  class="delete btn btn-danger btn-sm">Delete</a>';
                            return $btn;
                    })
                    ->rawColumns(['action'])
                    ->make(true);
        }
        return view('siswa.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $kelas = Kelas::all();
        return view('siswa.create', compact('kelas'));
        
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([ 
    		'nisn' => 'required',
    		'no_induk' => 'required',
            'name' => 'required',
    		'tempat_lahir' => 'required',
            'tanggal_lahir' => 'required',
    		'agama' => 'required',
            'alamat' => 'required',
    		'no_hp_wali' => 'required',
            'kelas_id' => 'required'
    	]);
        $post = new siswa;
        $post->nisn = $request["nisn"];
        $post->no_induk = $request["no_induk"];
        $post->name = $request["name"];
        $post->tempat_lahir = $request["tempat_lahir"];
        $post->tanggal_lahir = $request["tanggal_lahir"];
        $post->agama = $request["agama"];
        $post->alamat = $request["alamat"];
        $post->no_hp_wali = $request["no_hp_wali"];
        $post->kelas_id = $request["kelas_id"];
        $post->save();
        return redirect(route('siswa.index'))->with('success', 'Data Berhasil Di tambahkan');
    
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $siswa = siswa::find($id);
        return view('siswa.edit', ['siswa' => $siswa]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'nisn' => 'required',
    		'no_induk' => 'required',
            'name' => 'required',
    		'tempat_lahir' => 'required',
            'tanggal_lahir' => 'required',
    		'agama' => 'required',
            'alamat' => 'required',
    		'no_hp_wali' => 'required',
            'kelas_id' => 'required'
        ]);

        $siswa = siswa::find($id);
        $siswa->nisn = $request->nisn;
        $siswa->no_induk = $request->no_induk;
        $siswa->name = $request->name;
        $siswa->tempat_lahir = $request->tempat_lahir;
        $siswa->tanggal_lahir = $request->tanggal_lahir;
        $siswa->agama = $request->agama;
        $siswa->alamat = $request->alamat;
        $siswa->no_hp_wali = $request->no_hp_wali;
        $siswa->kelas_id = $request->kelas_id ;
        $siswa->update();
        Alert::success('Berhasil', 'Data Siswa Berhasil Di Perbaharui');
        
        return redirect(route('siswa.index'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $siswa = siswa::findOrFail($id);
        $siswa->delete();

        Alert::warning('Berhasil!', 'Data Berhasil Di Hapus');

        return redirect(route('siswa.index'));
    }
}
