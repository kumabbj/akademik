<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Kelas extends Model
{
    protected $table = "kelas";
    protected $fillable = ['id','nama'];
    public function siswa()
    {
    return $this->hasMany('App\Siswa');
    } 
    // protected $fillable = ['id','nama'];

    public function User(){
        return $this->hasMany(User::class);
    }
    protected $guarded = [];
}
