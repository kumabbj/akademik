<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class siswa extends Model
{
    protected $table = "siswas";
    protected $fillable = 
    ["nisn", 
    "body",
    "no_induk",
    "name",
    "tempat_lahir",
    "tanggal_lahir",
    "agama",
    "alamat",
    "no_hp_wali",
    "kelas_id"
    ];
    public function kelas(){
    return $this->belongsTo('App\Kelas');
    } 
}
