<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use App\Http\Middleware\ceklevel;

Route::get('/', function () {
    return view('auth.login');
});

Auth::routes();
Route::group(['middleware' => ['auth', 'ceklevel:admin,guru']], function (){
Route::get('/home', 'HomeController@index')->name('home');
Route::resource('user', 'UserController')->except('destroy');
Route::resource('kelas', 'KelasController')->except('destroy');
Route::get('register', 'auth\RegisterController@showRegistrationForm')->name('register');
Route::get('/kelas/destroy/{id}', 'KelasController@destroy')->name('kelas.destroy');
Route::get('/user/destroy/{id}', 'UserController@destroy')->name('user.destroy');
Route::resource('siswa', 'SiswaController')->except('destroy');
Route::get('/siswa/destroy/{id}', 'SiswaController@destroy')->name('siswa.destroy');
});

