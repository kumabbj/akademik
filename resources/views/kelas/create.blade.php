@extends('layouts.master')

@section('content')
<div class="ml-3 mr-3">
    <div class="card card-primary">
        <div class="card-header">
          <h3 class="card-title">Create New Cast</h3>
        </div>
        <!-- /.card-header -->
        <!-- form start -->
        <form  role="form" action="{{ route('kelas.store')}}" method="post">
            @csrf
          <div class="card-body">
            <div class="form-group">
              <label for="nama">Nama Kelas</label>
              <input type="text" class="form-control" id="name" name="name" value="{{old('name')}}"  placeholder="Masukan Name">
              @error('name')
              <div class="alert alert-danger">{{ $message }}</div>
              @enderror
            </div>
          <!-- /.card-body -->
    
          <div class="card-footer">
            <button type="submit" class="btn btn-primary">Create New</button>
          </div>
        </form>
      </div>
</div>
@endsection
