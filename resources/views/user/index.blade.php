@extends('layouts.master')

@section('content')
<div class="container-fluid">
    {{-- flash Massage --}}
    @if (session('success'))
    <div class="alert alert-warning alert-dismissible fade show" role="alert">
        {{ session('success') }}
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
        </button>
    </div>
    @endif

    <table class="table table-hover data-table">
        <thead>
            <tr>
                <th scope="col">No</th>
                <th scope="col">Username</th>
                <th scope="col">Nama</th>
                <th scope="col">Nip</th>
                <th scope="col">Email</th>
                <th scope="col">Alamat</th>
                <th scope="col">No_hp</th>
                <th scope="col">Level</th>
                <th scope="col">Kelas</th>
                <th scope="col">Action</th>
            </tr>
        </thead>
        <tbody>

        </tbody>
    </table>
</div>

@push('script')
<script type="text/javascript">
    $(function () {
      
      var table = $('.data-table').DataTable({
          processing: true,
          serverSide: true,
          ajax: "{{ route('user.index') }}",
          columns: [
            { "data": null,"sortable": false, 
            render: function (data, type, row, meta) {
            return meta.row + meta.settings._iDisplayStart + 1;}},
              {data: 'username', name: 'username'},
              {data: 'name', name: 'name'},
              {data: 'nip', name: 'nip'},
              {data: 'email', name: 'email'},
              {data: 'alamat', name: 'alamat'},
              {data: 'no_hp', name: 'no_hp'},
              {data: 'level', name: 'level'},
              {data: 'kelas', name: 'kelas.nama'},
              {data: 'action', name: 'action', orderable: false, searchable: false},
          ]
      });
      
    });
</script>
@endpush
@endsection
