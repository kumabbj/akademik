@extends('layouts.master')
@section('content')
<div class="container-fluid">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-body">
                    <form action="{{ route('siswa.update', [$siswa->id]) }}" method="POST">
                        @csrf
                        @method('PUT')
                        <div class="form-group">
                            <label for="nisn">NISN</label>
                            <input type="text" name="nisn" class="form-control" id="nisn" value="{{ $siswa->nisn }}"
                                placeholder="NISN.." autofocus>
                            @error('nisn')
                            <div class="alert alert-danger">
                                {{ $message }}
                            </div>
                            @enderror
                        </div>
                        <div class="form-group">
                            <label for="no_induk">No Induk</label>
                            <input type="text" name="no_induk" class="form-control" id="no_induk" value="{{ $siswa->no_induk }}"
                                placeholder="No Induk.." autofocus>
                            @error('no_induk')
                            <div class="alert alert-danger">
                                {{ $message }}
                            </div>
                            @enderror
                        </div>
                        <div class="form-group">
                            <label for="name">Nama</label>
                            <input type="text" name="name" class="form-control" id="name" value="{{ $siswa->name }}"
                                placeholder="Full Name.." autofocus>
                            @error('name')
                            <div class="alert alert-danger">
                                {{ $message }}
                            </div>
                            @enderror
                        </div>
                        <div class="form-group">
                            <label for="tempat_lahir">Tempat Lahir</label>
                            <input type="text" name="tempat_lahir" class="form-control" id="tempat_lahir" value="{{ $siswa->tempat_lahir }}"
                                placeholder="Tempat Lahir.." autofocus>
                            @error('tempat_lahir')
                            <div class="alert alert-danger">
                                {{ $message }}
                            </div>
                            @enderror
                        </div>
                        <div class="form-group">
                            <label for="tanggal_lahir">Tanggal Lahir</label>
                            <input type="text" name="tanggal_lahir" class="form-control" id="tanggal_lahir" value="{{ $siswa->tanggal_lahir }}"
                                placeholder="Tanggal Lahir.." autofocus>
                            @error('tanggal_lahir')
                            <div class="alert alert-danger">
                                {{ $message }}
                            </div>
                            @enderror
                        </div>
                        <div class="form-group">
                            <label for="agama">Agama</label>
                            <input type="text" name="agama" class="form-control" id="agama" value="{{ $siswa->agama }}"
                                placeholder="Agama.." autofocus>
                            @error('agama')
                            <div class="alert alert-danger">
                                {{ $message }}
                            </div>
                            @enderror
                        </div>
                        <div class="form-group">
                            <label for="alamat">Alamat</label>
                            <input type="text" name="alamat" class="form-control" id="alamat" value="{{ $siswa->alamat }}"
                                placeholder="Full Name.." autofocus>
                            @error('alamat')
                            <div class="alert alert-danger">
                                {{ $message }}
                            </div>
                            @enderror
                        </div>
                        <div class="form-group">
                            <label for="no_hp_wali">No HP wali</label>
                            <input type="text" name="no_hp_wali" class="form-control" id="no_hp_wali" value="{{ $siswa->no_hp_wali }}"
                                placeholder="Full Name.." autofocus>
                            @error('no_hp_wali')
                            <div class="alert alert-danger">
                                {{ $message }}
                            </div>
                            @enderror
                        </div>
                        <div class="form-group">
                            <label for="kelas_id">Kelas</label>
                            <input type="text" name="kelas_id" class="form-control" id="kelas_id" value="{{ $siswa->kelas_id }}"
                                placeholder="Kelas.." autofocus>
                            @error('kelas_id')
                            <div class="alert alert-danger">
                                {{ $message }}
                            </div>
                            @enderror
                        </div>
                        <button type="submit" class="btn btn-outline-primary float-right">Update</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection