@extends('layouts.master')
@section('content')
<h2>Tambah Data</h2>
<div class="ml-3 mr-3">
    <div class="card card-primary">
        <div class="card-header">
          <h3 class="card-title">Buat Data Siswa</h3>
        </div>
        <!-- /.card-header -->
        <!-- form start -->
        <form  role="form" action="{{ route('siswa.store')}}" method="post">
        @csrf
          <div class="card-body">
            <div class="form-group">
              <label for="nisn">NISN</label>
              <input type="text" class="form-control" id="nisn" name="nisn" value="{{old('nisn')}}"  placeholder="Masukan Nama">
              @error('nisn')
              <div class="alert alert-danger">{{ $message }}</div>
              @enderror
            </div>
            <div class="form-group">
                <label for="nisn">NISN</label>
                <input type="text" class="form-control" id="nisn" name="nisn" value="{{old('nisn')}}"  placeholder="Masukan Nama">
                @error('nisn')
                <div class="alert alert-danger">{{ $message }}</div>
                @enderror
              </div>
              <div class="form-group">
                <label for="no_induk">No Induk</label>
                <input type="text" class="form-control" id="no_induk" name="no_induk" value="{{old('no_induk')}}"  placeholder="Masukan No Induk">
                @error('no_induk')
                <div class="alert alert-danger">{{ $message }}</div>
                @enderror
              </div>
              <div class="form-group">
                <label for="name">Nama</label>
                <input type="text" class="form-control" id="name" name="name" value="{{old('name')}}"  placeholder="Masukan Nama">
                @error('name')
                <div class="alert alert-danger">{{ $message }}</div>
                @enderror
              </div>
              <div class="form-group">
                <label for="tempat_lahir">Tempat Lahir</label>
                <input type="text" class="form-control" id="tempat_lahir" name="tempat_lahir" value="{{old('tempat_lahir')}}"  placeholder="Masukan Tempat Lahir">
                @error('tempat_lahir')
                <div class="alert alert-danger">{{ $message }}</div>
                @enderror
              </div>
              <div class="form-group">
                <label for="tanggal_lahir">Tanggal Lahir</label>
                <div class="input-group date" id='datetimepicker1'>
                    <span class="input-group-addon">
                    <span class="glyphicon glyphicon-calendar"></span>
                    </span>
                    <input type="text" class="form-control" name="tanggal_lahir" id="tanggal_lahir" value="{{old('tanggal_lahir')}}" placeholder="yyyy/mm/dd">
                  </div>
                @error('tanggal_lahir')
                <div class="alert alert-danger">{{ $message }}</div>
                @enderror
              </div>
              <div class="form-group">
                <label for="agama">Agama</label>
                <input type="text" class="form-control" id="agama" name="agama" value="{{old('agama')}}"  placeholder="Masukan Agama">
                @error('agama')
                <div class="alert alert-danger">{{ $message }}</div>
                @enderror
              </div>
              <div class="form-group">
                <label for="alamat">NISN</label>
                <input type="text" class="form-control" id="alamat" name="alamat" value="{{old('alamat')}}"  placeholder="Masukan Alamat">
                @error('alamat')
                <div class="alert alert-danger">{{ $message }}</div>
                @enderror
              </div>
              <div class="form-group">
                <label for="no_hp_wali">No HP Wali</label>
                <input type="text" class="form-control" id="no_hp_wali" name="no_hp_wali" value="{{old('no_hp_wali')}}"  placeholder="Masukan No HP Wali">
                @error('no_hp_wali')
                <div class="alert alert-danger">{{ $message }}</div>
                @enderror
              </div>
              <div class="form-group">
                <label for="kelas_id">ID Kelas</label>
                <div class="col-md-6">
                  <select name="kelas_id" id="kelas_id" class="form-control"  class="form-control @error('kelas_id') is-invalid @enderror" value="{{ old('kelas_id') }}" required autocomplete="Kelas" autofocus>
                      <option value="" selected>---Pilih Kelas---</option>
                      @foreach ($kelas as $item)
                      <option value="{{$item->id}}">{{$item->nama}}</option>
                      @endforeach
                  </select>
                  <br>
                @error('kelas_id')
                <div class="alert alert-danger">{{ $message }}</div>
                @enderror
              </div>
          <!-- /.card-body -->
    
          <div class="card-footer">
            <button type="submit" class="btn btn-primary">Create New</button>
          </div>
        </form>
      </div>
</div>
@endsection
