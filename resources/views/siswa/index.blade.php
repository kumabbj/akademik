@extends('layouts.master')

@section('content')
<div class="container-fluid">
    {{-- flash Massage --}}
    @if (session('success'))
    <div class="alert alert-warning alert-dismissible fade show" role="alert">
        {{ session('success') }}
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
        </button>
    </div>
    @endif

    <table class="table table-hover data-table">
        <thead>
            <tr>
                <th scope="col">ID</th>
                <th scope="col">NISN</th>
                <th scope="col">NO Induk</th>
                <th scope="col">Nama</th>
                <th scope="col">Tempat Lahir</th>
                <th scope="col">Tanggal Lahir</th>
                <th scope="col">Agama</th>
                <th scope="col">Alamat</th>
                <th scope="col">NO HP</th>
                <th scope="col">Kelas ID</th>
                <th scope="col">Kelas</th>
                <th scope="col">Action</th>
            </tr>
        </thead>
        <tbody>
        </tbody>
    </table>
</div>

@push('script')
<script type="text/javascript">
    $(function () {
      
      var table = $('.data-table').DataTable({
          processing: true,
          serverSide: true,
          ajax: "{{ route('siswa.index') }}",
          columns: [
              {data: 'id', name: 'id'},
              {data: 'nisn', name: 'nisn'},
              {data: 'no_induk', name: 'no_induk'},
              {data: 'name', name: 'name'},
              {data: 'tempat_lahir', name: 'tempat_lahir'},
              {data: 'tanggal_lahir', name: 'tanggal_lahir'},
              {data: 'agama', name: 'agama'},
              {data: 'alamat', name: 'alamat'},
              {data: 'no_hp_wali', name: 'no_hp_wali'},
              {data: 'kelas_id', name: 'Kelas.name'},
              {data: 'Kelas', name: 'Kelas.name'},
              {data: 'action', name: 'action', orderable: false, searchable: false},
          ]
      });
      
    });
</script>
@endpush
@endsection